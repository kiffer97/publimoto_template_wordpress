<footer class="main-footer">

    <div class="widgets-section">
        <div class="container">
            <div class="row justify-content-center">

                <!-- <div class="footer-column col-lg-3 col-md-6 col-sm-12">
                    <div class="footer-widget about-widget">
                        <figure class="footer-logo">
                            <a href="index.html"><img
                                    src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/logo2.png"
                                    alt=""></a>
                        </figure>

                        <div class="widget-content">
                            <div class="text">
                                <p>Aspla is a global consulting firm with a twist. We bring big ideas and challenge the
                                    norm. We work with our clients, not at them.</p>
                            </div>
                            <ul class="social">
                                <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-skype"></i></a></li>
                            </ul>
                        </div>
                        
                    </div>
                </div> -->

                <div class="footer-column col-lg-3 col-md-6 col-sm-12">
                    <div class="footer-widget service-widget">
                        <div class="section-title">
                            <h3>Acerca de </h3>
                        </div>
                        <div class="widget-content">
                            <ul class="list">
                                <li><a href="service-single.html">Business Growth</a></li>
                                <li><a href="service-single.html">Market Research</a></li>
                                <li><a href="service-single.html">Financial Advise</a></li>
                                <li><a href="service-single.html">Sales & Trading</a></li>
                                <li><a href="service-single.html">Consulting</a></li>
                                <li><a href="service-single.html">Risk Management</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="footer-column col-lg-4 col-md-6 col-sm-12">
                    <div class="footer-widget links-widget">
                        <div class="section-title">
                            <h3>Redes sociales</h3>
                        </div>
                      
                        <div class="widget-content">
                            <div class="text">
                                <!-- <p>Aspla is a global consulting firm with a twist. We bring big ideas and challenge the
                                    norm. We work with our clients, not at them.</p> -->
                            </div>
                            <ul class="social">
                                <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-skype"></i></a></li>
                            </ul>
                        </div>
                      
                    </div>
                </div>

                <div class="footer-column col-lg-3 col-md-6 col-sm-12">
                    <div class="footer-widget service-widget">
                        <div class="section-title">
                            <h3>Formas de pago </h3>
                        </div>
                        <div class="widget-content">
                            <ul class="list">
                                <li><a >Visa</a></li>
                                <li><a >Transferencia</a></li>
                                <li><a >Master Card</a></li>
                                <li><a >Efectivo</a></li>
                                <li><a >Depósito</a></li>
                                <li><a >Cheque</a></li>
                                <li><a >American Express</a></li>
                            </ul>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="copy-text text-center">
                        <p>Copyrights © 2020 Todos los derechos reservados <a target="_blank"
                                href="https://c-developersmx.com/"> C-developers</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<button class="scroll-top tran3s color2_bg">
    <span class="fa fa-long-arrow-up"></span>
</button>

<div class="preloader"></div>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-migrate-3.0.0.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/popper.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/bootstrap-select.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.bootstrap-touchspin.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-ui.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/owl.carousel.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.validate.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.mixitup.min.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.fancybox.pack.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/nouislider.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/isotope.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.appear.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.countTo.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/imagezoom.js"></script>

<script src="<?php echo get_template_directory_uri(); ?>/assets/js/custom.js"></script>
</div>
<?php wp_footer(); ?>

</body>

</html>