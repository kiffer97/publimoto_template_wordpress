<?php /* Template Name: Inicio */ ?>
<?php get_header(); ?>

<section class="main-slider">
    <div class="theme-slider owl-carousel owl-theme">

        <?php

        // Check rows exists.
        if( have_rows('anadir_nuevo_slider') ):

            // Loop through rows.
            while( have_rows('anadir_nuevo_slider') ) : the_row();

                // Load sub field value.
                $titulo     = get_sub_field('titulo');
                $sub_titulo = get_sub_field('sub-titulo');
                $img        = get_sub_field('imgen_del_slider');
            ?>
                <div class="slide-item">
                    <img src="<?php echo $img['url'];?>" alt="slide">
                    <div class="slide-overlay">
                        <div class="slide-table">
                            <div class="slide-table-cell">
                                <div class="container">
                                    <div class="slide-content">
                                        <h2><?php echo $titulo; ?></h2>
                                        <p><?php echo $sub_titulo; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            // End loop.
            endwhile;

            // No value.
            else :
            // Do something...
            endif;
            ?>

      

    </div>
</section>

<section class="working-process padd-1">
    <div class="container">

        <div class="sec-title center">
            <h2>Nuestro proceso de servicio</h2>
        </div>
        <div class="row">
            <?php
                // Check rows exists.
                if( have_rows('anadir_procesos_de_compra') ):
                    $contador    =1;
                    // Loop through rows.
                    while( have_rows('anadir_procesos_de_compra') ) : the_row();

                        // Load sub field value.
                        $titulo      = get_sub_field('titulo');
                        $sub_titulo  = get_sub_field('sub-titulo');

                        // Do something...
            ?>
                    <div class="process-block col-md-4 col-sm-12 col-xs-12">
                        <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                            <div class="count-box">
                                <div class="count">0<?php echo $contador;?></div>
                            </div>
                            <h3><?php echo $titulo;?></h3>
                            <div class="text"><?php echo $sub_titulo;?></div>
                        </div>
                    </div>
                    
            <?php
                // End loop.
                $contador++;
            endwhile;

            // No value.
            else :
                // Do something...
            endif;
            ?>
            <!-- <div class="process-block col-md-4 col-sm-12 col-xs-12">
                <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="count-box">
                        <div class="count">02</div>
                    </div>
                    <h3>Solicita tu pedido</h3>
                    <div class="text">We are leading international Business consulting all trouble with the law since
                        the the day what might be right.</div>
                   
                </div>
            </div>
            <div class="process-block col-md-4 col-sm-12 col-xs-12">
                <div class="inner-box wow fadeInLeft" data-wow-delay="0ms" data-wow-duration="1500ms">
                    <div class="count-box">
                        <div class="count">03</div>
                    </div>
                    <h3>Completa el pago</h3>
                    <div class="text">We are leading international Business consulting all trouble with the law since
                        the the day what might be right.</div>
                   
                </div>
            </div> -->

        </div>
    </div>
</section>

<section class="about-style1-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-12">
                <div class="about-style1-content">
                    <div class="sec-title style-2">
                        <h2>Acerca de <span>Publimoto</span></h2>
                    </div>
                    <div class="inner-content">
                        <div class="text">
                        <?php the_field('texto_acerca_de'); ?>
                            <!-- <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has
                                survived not only five centuries, but also the leap into electronic typesetting,
                                remaining essentially unchanged.</p>
                            <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem
                                Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker
                                including versions of Lorem Ipsum.</p> -->
                        </div>
                        <div class="row">

                            <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                                <div class="single-fact-counter wow fadeInUp" data-wow-delay="100ms"
                                    data-wow-duration="1500ms">
                                    <div class="count-box">
                                        <div class="count">
                                            <h2>
                                                <span class="timer" data-from="1" data-to="<?php the_field('porcentaje_1'); ?>" data-speed="5000"
                                                    data-refresh-interval="50"><?php the_field('porcentaje_1'); ?></span>
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </h2>
                                        </div>
                                        <div class="title">
                                            <h3>Proyectos Realizados</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                                <div class="single-fact-counter wow fadeInUp" data-wow-delay="200ms"
                                    data-wow-duration="1500ms">
                                    <div class="count-box">
                                        <div class="count">
                                            <h2>
                                                <span class="timer" data-from="1" data-to="<?php the_field('porcentaje_2'); ?>" data-speed="5000"
                                                    data-refresh-interval="50"><?php the_field('porcentaje_2'); ?></span>
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </h2>
                                        </div>
                                        <div class="title">
                                            <h3>Municipios atendidos</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-lg-4 col-md-4 col-sm-4 col-4">
                                <div class="single-fact-counter wow fadeInUp" data-wow-delay="300ms"
                                    data-wow-duration="1500ms">
                                    <div class="count-box">
                                        <div class="count">
                                            <h2>
                                                <span class="timer" data-from="1" data-to="<?php the_field('porcentaje_3'); ?>" data-speed="5000"
                                                    data-refresh-interval="50"><?php the_field('porcentaje_3'); ?></span>
                                                <i class="fa fa-percent" aria-hidden="true"></i>
                                            </h2>
                                        </div>
                                        <div class="title">
                                            <h3>Clientes satisfechos</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xl-12">
                                <ul class="button">
                                    <li><a href="/acerca-de"><span
                                                class="icon-arrow fa fa-angle-double-right"></span>Más acerca de Publimoto</a></li>
                                    <!-- <li><a href="/contactanos"><span class="icon-arrow fa fa-angle-double-right"></span>Contáctanos</a></li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-12">
                <div class="about-style1-right-content clearfix">
                    <?php $image_acerca_de=get_field('imagen_acerca_de');?>
                    <!-- <div class="about-style1-image-bg" style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/resource/about-style1-bg.jpg);"> -->
                    <div class="about-style1-image-bg" style="background-image:url(<?php echo $image_acerca_de['url']; ?>">
                    </div>
                    <div class="inner-content-box wow fadeInUp" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="icon-holder"><span class="icon fa fa-quote-right"></span></div>
                        <p>Expertos en<br> publicidad<br> comercial</p>
                        <h6><span>Director ejecutivo</span> Guillerno del Toro</h6>
                        <div class="signature">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/resource/signature.png"
                                alt="Signature">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="services-style-one padd-1 bg-1">
    <div class="container">
        <div class="sec-title center">
            <h2>Nuestros <span>Servicios</span></h2>
        </div>
        <div class="row clearfix">
                <?php
                // Check rows exists.
                if( have_rows('agregar_nuevo_servicio') ):

                    // Loop through rows.
                    while( have_rows('agregar_nuevo_servicio') ) : the_row();

                        // Load sub field value.
                        $titulo     = get_sub_field('titulo');
                        $texto      = get_sub_field('texto');
                        $imagen     = get_sub_field('imagen_del_servicio');
                ?>
                <div class="column col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="service-block wow fadeIn" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="inner-box">
                            <div class="image-box">
                                <!-- <img src="<?php echo get_template_directory_uri(); ?>/assets/images/service/service-1.jpg"alt="" /> -->
                                <img src="<?php echo $imagen['url'] ?>"alt="" />
                                <div class="caption"><?php  echo $titulo; ?></div>
                                <div class="overlay-box">
                                    <h3><?php echo $titulo;?></h3>
                                    <div class="text"><?php echo $texto;?>
                                    </div>
                                    <!-- <a class="btn-style-three" href="service-single.html">Read More <span
                                            class="fa fa-angle-double-right"></span></a> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                // End loop.
                endwhile;

                // No value.
                else :
                    // Do something...
                endif;
                ?>
        </div>
    </div>
</section>


<section class="testimonials-section padd-2"
    style="background-image:url(<?php echo get_template_directory_uri(); ?>/assets/images/background/4.jpg);">
    <div class="container">
        <div class="sec-title light center">
            <h2>Comentarios de <span>clientes</span></h2>
        </div>

        <div class="four-item-carousel owl-carousel owl-theme owl-nav-style-one owl-dot-none">
            <?php

            // Check rows exists.
            if( have_rows('anadir_nuevos_comentarios') ):

                // Loop through rows.
                while( have_rows('anadir_nuevos_comentarios') ) : the_row();

                    // Load sub field value.
                    $comentario = get_sub_field('comentario');
                    $nombre     = get_sub_field('nombre');
                    $lugar      = get_sub_field('lugar');
                    $imagen     = get_sub_field('imagen');
                    // Do something...
                    ?>
                    <article class="testi-slide-item">
                        <div class="slide-text">
                            <p>“<?php echo $comentario;?>”</p>
                        </div>
                        <div class="info-box">
                            <figure class="image-box"><img src="<?php echo $imagen['url'];?>" alt="">
                            </figure>
                            <h3><?php echo $nombre; ?></h3>
                            <p class="designation"><?php echo $lugar;?> </p>
                        </div>
                    </article>
                    <?php

                // End loop.
                endwhile;

            // No value.
            else :
                // Do something...
            endif;
            ?>


        </div>
    </div>
</section>

<section class="clients-section padd-2">
    <div class="container">
        <div class="sec-title center">
            <h2>Nuestros <span>clientes</span></h2>
        </div>
        <div class="client-carousel owl-carousel owl-theme">
            <?php
            if( have_rows('anadir_nuevos_clientes') ):

                // Loop through rows.
                while( have_rows('anadir_nuevos_clientes') ) : the_row();

                    // Load sub field value.
                    $nombre_del_cliente = get_sub_field('nombre_del_cliente');
                    $imagen_del_cliente = get_sub_field('imagen_del_cliente');
                    // Do something...
            ?>
                    <div class="item tool_tip" title="<?php echo $nombre_del_cliente;?>">
                        <img src="<?php echo $imagen_del_cliente['url']; ?>" alt="Awesome Image">
                    </div>
            <?php
                // End loop.
                endwhile;

            // No value.
            else :
                // Do something...
            endif;            
            ?>       
            

    </div>
</section>

<?php get_footer();?>