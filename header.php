<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta name="author" content="Crescent Theme" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="keywords" content="html, html5, SEO, responsive, creative, fullscreen, business, samll business, multi purpose, bootstrap, bootstrap 4, google material design, material design, metro, metronic, start-up, ui/ux, html template, css3, css, branding, creative design, multipurpose, multi-purpose, parallax, personal, consulting, masonry, grid, carousel, agency, construction, ecommerce, landing, event, medical,eal-estate">
<meta name="description" content="Aspla is Professional Creative Template. Aspla is a Powerful Responsive Multi-Purpose HTML5 CSS3 Template. Build whatever you like with this Template. We have included 6+ ready-to-use Homepages. What ever you are using the device your site will run as it should be. This template is fully responsive layout for all type of devices. Aspla template coded with beautiful and clean codes! Some powerful HTML files 100% valid W3 web standards. Try out the demo." />
<meta property="og:title" content="Aspla is a Powerful Creative Responsive Multi-Purpose HTML5 Template" />

<title>Publimoto</title>

<link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicon/favicon-1.png">
<!-- <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-2.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon/favicon-3.png" sizes="16x16"> -->
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/logo/logo.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/images/logo/logo.png" sizes="16x16">

<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/bootstrap-select.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/jquery.bootstrap-touchspin.css">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/font-awesome.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/flaticon.css">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/owl.carousel.css">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/jquery-ui.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/jquery.fancybox.css">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/animate.min.css">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/nouislider.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/nouislider.pips.css">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/magnific-popup.css">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/style.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/responsive.css">
</head>
<body>
<div class="boxed_wrapper">

<header class="top-bar">
    <div class="container">
        <div class="clearfix">
            <ul class="top-bar-text float_left  d-none d-md-block">
                <li class="top-bar-phone"><i class="flaticon-telephone"></i><a href="tel:1234567890"> Tel +123-456-7890</a></li>
                <li class="top-bar-mail"><i class="flaticon-envelope"></i><a href="mailto:yourcompany@gmail.com">publimoto@gmail.com</a></li>
            </ul>
            <ul class="social-style-one float_right">
                <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="javascript:void(0)"><i class="fa fa-skype"></i></a></li>
            </ul>
        </div>
    </div>
<?php wp_head();?>

</header>


<section class="mainmenu-area stricky">
    <div class="container">
        <div class="">
            <nav class="navbar main-menu navbar-expand-lg p-0">
            <a class="navbar-brand" href="/inicio">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo/logo.png" alt="" style="width:160px;height:60px">
            </a>
            <button class="navbar-toggler" type="button">
            <span class="navbar-toggler-icon bar1"></span>
            <span class="navbar-toggler-icon bar2"></span>
            <span class="navbar-toggler-icon bar3"></span>
            </button>
            <div class="collapse navbar-collapse" id="sitemenu">
                <ul class="navigation main_menu ml-auto clearfix">
                <li class="current dropdown"><a href="/inicio">Inicio</a>
                <ul>
                    <!-- <li><a href="index.html">Home One</a></li>
                    <li><a href="index-2.html">Home Two</a></li>
                    <li><a href="index-3.html">Home Three</a></li>
                    <li><a href="index-4.html">Home Four</a></li>
                    <li><a href="index-5.html">Home Five</a></li>
                    <li><a href="index-6.html">Home Six</a></li> -->
                </ul>
                </li>
                <!-- <li class="dropdown"><a href="javascript:void(0)">About</a> -->
                <li class="dropdown"><a href="/acerca-de">Acerca de</a>
                <ul>
                    <!-- <li><a href="about.html">About us</a></li>
                    <li><a href="team.html">Team</a></li>
                    <li><a href="testimonials.html">Testimonials</a></li>
                    <li><a href="error.html">404 Page</a></li> -->
                </ul>
                </li>
                <!-- <li class="dropdown"><a href="javascript:void(0)">Projects</a>
                    <ul>
                        <li><a href="our-projects.html">projects</a></li>
                        <li><a href="project-single.html">Project Details</a></li>
                    </ul>
                </li> -->
                <!-- <li class="dropdown"><a href="javascript:void(0)">services</a>
                    <ul>
                        <li><a href="service.html">Services</a></li>
                        <li><a href="service-single.html">Service Details</a></li>
                    </ul>
                </li> -->
                <!-- <li class="dropdown"><a href="javascript:void(0)">News</a>
                    <ul>
                        <li><a href="blog-grid.html">News Grid</a></li>
                        <li><a href="blog-large.html">News Classic</a></li>
                        <li><a href="blog-details.html">News Details</a></li>
                    </ul>
                </li> -->
                <!-- <li><a href="contact.html">Contact</a></li> -->
                </ul>
                <div class="quote-btn my-2 my-md-0 d-none d-lg-block">
                    <!-- <a href="javascript:void(0)" class="btn-style-one">Get A Quote</a> -->
                </div>
            </div>
            </nav>
        </div>
    </div>
</section>