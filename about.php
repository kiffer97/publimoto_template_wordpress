<?php /* Template Name: Acerca */ ?>
<?php get_header(); ?>

<div class="inner-banner has-base-color-overlay text-center"
    style="background: url(<?php echo get_template_directory_uri(); ?>/assets/images/background/1.jpg);">
    <div class="container">
        <div class="box">
            <h3>Acerca de</h3>
        </div>
    </div>
</div>

<div class="breadcumb-wrapper">
    <div class="container">
        <div class="breadcrumb_bar">
            <div class="breadcrumb_menu">
                <ul class="link-list">
                    <li>
                        <a href="/inicio">Inicio</a>
                    </li>
                    <li>
                        Acerca de
                    </li>
                </ul>

            </div>
        </div>

    </div>

</div>


<!-- <section class="feature-style-one padd-1">
            <div class="container">
                <div class="sec-title center">
                    <h2>Make Successful Business</h2>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 feature-column">
                        <div class="feature-content-one">
                            <div class="hexagon-content">
                                <div class="hexagon">
                                    <div class="icon"><i class="flaticon-presentation"></i></div>
                                </div>
                            </div>
                            <div class="lower-content">
                                <div class="title">
                                    <h4><a href="#">Market Analysis</a></h4>
                                </div>
                                <div class="text">We are leading international Business consulting all trouble with the
                                    law since the the day what might be right.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 feature-column">
                        <div class="feature-content-one">
                            <div class="hexagon-content">
                                <div class="hexagon">
                                    <div class="icon"><i class="flaticon-shield"></i></div>
                                </div>
                            </div>
                            <div class="lower-content">
                                <div class="title">
                                    <h4><a href="#">Business Development</a></h4>
                                </div>
                                <div class="text">We are leading international Business consulting all trouble with the
                                    law since the the day what might be right.</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12 feature-column">
                        <div class="feature-content-one">
                            <div class="hexagon-content">
                                <div class="hexagon">
                                    <div class="icon"><i class="flaticon-people"></i></div>
                                </div>
                            </div>
                            <div class="lower-content">
                                <div class="title">
                                    <h4><a href="#">Full Support</a></h4>
                                </div>
                                <div class="text">We are leading international Business consulting all trouble with the
                                    law since the the day what might be right.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->


<section class="who-we-are-area padd-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="who-we-are-img-holder">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/resource/who_we_are.jpg"
                        alt="Awesome Image" class="wow fadeIn" data-wow-duration="2s" data-wow-delay="0s"
                        data-wow-offset="0">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div class="content">
                    <div class="sec-title style-2">
                        <h2>¿Quienes <span>somos?</span></h2>
                    </div>

                    <div class="text" style=" text-align: justify; text-justify: inter-word;">
                        <?php echo the_field('texto_principal_quienes_somos'); ?>
                    </div>

                    <div class="who-item-carousel owl-carousel owl-theme owl-nav-style-one owl-dot-none">
                        <?php

                                // Check rows exists.
                                if( have_rows('subtemas_quienes_somos') ):

                                    // Loop through rows.
                                    while( have_rows('subtemas_quienes_somos') ) : the_row();

                                        // Load sub field value.
                                        $tema = get_sub_field('tema');
                                        $texto = get_sub_field('texto');
                                        $imagen_del_tema = get_sub_field('imagen_del_tema');
                                        ?>
                                        <div class="who_test">
                                            <div class="founder clearfix">
                                                <div class="img-holder">
                                                    <img src="<?php echo $imagen_del_tema['url']; ?>" alt="Awesome Image"
                                                        style="width:85px;height:85px">
                                                </div>
                                                <div class="name">
                                                    <h3> <?php echo $tema;?> </h3>
                                                </div>
                                            </div>
                                            <div class="text">
                                                <?php echo $texto;?>
                                            </div>
                                        </div>
                                        <?php

                                    // End loop.
                                    endwhile;

                                // No value.
                                else :
                                    // Do something...
                                endif;
                            ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="what-we-do about-we-do">
    <div class="container">
        <div class="sec-title center">
            <!-- <h2>What <span>We Do</span></h2> -->
            <h2>Nuestros <span>Servicios</span></h2>
        </div>

        <div class="features-one">
            <div class="row clearfix">
                <?php

                        // Check rows exists.
                        if( have_rows('nuestros_servicios') ):

                            // Loop through rows.
                            while( have_rows('nuestros_servicios') ) : the_row();

                                // Load sub field value.
                                $titulo_del_servicio    = get_sub_field('titulo_del_servicio');
                                $texto_del_servicio     = get_sub_field('texto_del_servicio');
                                $imagen_del_servicio    = get_sub_field('imagen_del_servicio');
                                $icono="";
                                switch ($imagen_del_servicio) {
                                    case "analisis":
                                        $icono="flaticon-money";
                                        break;
                                    case "ingresos":
                                        $icono="flaticon-note";
                                        break;
                                    case "graficas":
                                        $icono="flaticon-connection";
                                        break;
                                    case "consultoria":
                                        $icono="flaticon-people";
                                        break;
                                    case "rendimiento":
                                        $icono="flaticon-layers";
                                        break;
                                    case "asesoramiento":
                                        $icono="flaticon-business";
                                        break;
                                }
                            ?>
                                <div class="column left-icon-column col-md-4 col-sm-6 col-xs-12">
                                    <div class="inner-box">
                                        <div class="icon"><span class="<?php echo $icono; ?>"></span></div>
                                        <h3> <?php echo $titulo_del_servicio;?> </h3>
                                        <div class="text">
                                            <?php echo $texto_del_servicio;?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            // End loop.
                            endwhile;

                        // No value.
                        else :
                            // Do something...
                        endif;
                    ?>


            </div>
        </div>
    </div>
</section>


<section class="our-skill padd-5">
    <div class="container">
        <div class="sec-title">
            <h2> <span>Skills</span></h2>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">

                <div class="video-image-box">
                    <figure class="image">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/resource/about2.jpg" alt="">
                        <a href="https://www.youtube.com/watch?v=G0dzLanYW1E&amp;t=28s"
                            class="overlay-link lightbox-image video-fancybox"><span class="fa fa-play"></span></a>
                    </figure>
                </div>
            </div>

            <div class="skills-column col-md-6 col-sm-6">

                <div class="progress-levels">

                    <div class="progress-box wow" data-wow-delay="100ms" data-wow-duration="1500ms">
                        <div class="box-title">Business Consulting</div>
                        <div class="inner">
                            <div class="bar">
                                <div class="bar-innner">
                                    <div class="bar-fill" data-percent="78">
                                        <div class="percent"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="progress-box wow" data-wow-delay="200ms" data-wow-duration="1500ms">
                        <div class="box-title">Lawyers Consulting</div>
                        <div class="inner">
                            <div class="bar">
                                <div class="bar-innner">
                                    <div class="bar-fill" data-percent="90">
                                        <div class="percent"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="progress-box wow" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="box-title">Online Consulting</div>
                        <div class="inner">
                            <div class="bar">
                                <div class="bar-innner">
                                    <div class="bar-fill" data-percent="85">
                                        <div class="percent"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="progress-box wow" data-wow-delay="300ms" data-wow-duration="1500ms">
                        <div class="box-title">Financial Planing</div>
                        <div class="inner">
                            <div class="bar">
                                <div class="bar-innner">
                                    <div class="bar-fill" data-percent="65">
                                        <div class="percent"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer();?>